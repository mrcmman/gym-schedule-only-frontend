import React, { Component } from 'react';

import {networkUtill} from "./networkUtill";
import logo from './logo.svg';
import './App.css';
import MaterialTable from "material-table";
import { forwardRef } from 'react';

import AddBox from '@material-ui/icons/AddBox';
import ArrowDownward from '@material-ui/icons/ArrowDownward';
import Check from '@material-ui/icons/Check';
import ChevronLeft from '@material-ui/icons/ChevronLeft';
import ChevronRight from '@material-ui/icons/ChevronRight';
import Clear from '@material-ui/icons/Clear';
import DeleteOutline from '@material-ui/icons/DeleteOutline';
import Edit from '@material-ui/icons/Edit';
import FilterList from '@material-ui/icons/FilterList';
import FirstPage from '@material-ui/icons/FirstPage';
import LastPage from '@material-ui/icons/LastPage';
import Remove from '@material-ui/icons/Remove';
import SaveAlt from '@material-ui/icons/SaveAlt';
import Search from '@material-ui/icons/Search';
import ViewColumn from '@material-ui/icons/ViewColumn';

const tableIcons = {
    Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
    Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
    Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
    DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
    Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
    Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
    FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
    LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
    NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
    PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
    ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
    Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
    SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
    ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
    ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
};

class App extends Component {

    constructor(props) {
        super(props);
        this.handleChangeName = this.handleChangeName.bind(this);
        this.handleChangeCost = this.handleChangeCost.bind(this);
        this.handleChangeDesc = this.handleChangeDesc.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state={data: [],name:'',cost:'',desc:''};
    }
    async componentDidMount() {
     await this.gettable();
    }
    async gettable()
    {
        const url = 'http://localhost:8080/fourth/api/gettable';
        const requestOptions = {
            method: 'GET',
            headers: {'Content-Type': 'application/json;charset=utf-8'}
        };
        const res = await networkUtill(url, requestOptions);
        console.log(res);
        this.setState({data: res});
    }
    async deleteData(id) {
        const url = 'http://localhost:8080/fourth/api/deleterow?id='+id;
        const requestOptions = {
            method: 'DELETE',
            headers: {'Content-Type': 'application/json;charset=utf-8'}
        };
        const res = await networkUtill(url, requestOptions);
        console.log(res);
        this.gettable();
        alert(res);
    }
    handleChangeName(event) {
        this.setState({name: event.target.value});
    }
    handleChangeCost(event) {
        this.setState({cost: event.target.value});
    }
    handleChangeDesc(event) {
        this.setState({desc: event.target.value});
    }

   async handleSubmit(event) {
        if( (this.state.name!=='')&&(this.state.cost!=='')&&(this.state.desc!=='')) {
            const url = 'http://localhost:8080/fourth/api/putrow?name=' + this.state.name + '&cost=' + this.state.cost + '&desc=' + this.state.desc;
            const requestOptions = {
                method: 'PUT',
                headers: {'Content-Type': 'application/json;charset=utf-8'}
            };
            const res = await networkUtill(url, requestOptions);
            console.log(res);
            this.gettable();
            alert(res);
        }
        else {
            alert("Не все данные введены")
        }
    }
    render() {
    return (
      <div className="App">
        <div style={{ display: 'flex',  justifyContent:'left', alignItems:'center', Width: "130px",padding: "10px", backgroundColor: "#222"}}>
          <img src={logo} className="App-logo" alt="logo" />
          <h3 style={{color:"#fff", marginLeft:15}}>Спортивный зал </h3>
        </div>

          <div style={{ display: 'flex',  justifyContent:'center', alignItems:'center', Width: "100%" ,flex:1}}>
              <MaterialTable
                  icons={tableIcons}
                  actions={[
                       {
                          icon: () => <DeleteOutline  />,
                          onClick: (event, rowData) => {
                                this.deleteData(rowData.id);
                          }
                      }
                  ]}
                  columns={[
                      { title: "id", field: "id" },
                      { title: "name", field: "name" },
                      { title: "cost", field: "cost"},
                      { title: "desc", field: "desc"},

                  ]}
                  data={this.state.data}
                  title="Products"
              />

          </div>
          <div>

              <form onSubmit={this.handleSubmit}>
                  <label>
                      Название:
                      <input type="text" value={this.state.name} onChange={this.handleChangeName} />
                  </label>
                  <label>
                      Цена:
                      <input type="text" value={this.state.cost} onChange={this.handleChangeCost} />
                  </label>
                  <label>
                      Описание:
                      <input type="text" value={this.state.desc} onChange={this.handleChangeDesc} />
                  </label>
                  <input type="submit" value="Отправить" />
              </form>

          </div>
          </div>

    );
  }
}





export default App;
